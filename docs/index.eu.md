---
subtitle: Apunteak, laburpena, presentazioak eta webgunea!
affiliation: Matematika Aplikatua saila (UPV-EHU)
itinerary:
  a: 
    title: Ingeniaritza Mekanikoa
  b: 
    title: Ingeniaritza Elektronikoa
---

Eduki Markdown bitartez apunteak sortzeko herraminta bat da.
Edukiren filosofia hurrengo esaldian bildu daiteke:

*Kode base bat ...*

![
  *Un Anillo para gobernarlos a todos.<br> Un Anillo para encontrarlos,<br>
un Anillo para atraerlos a todos y atarlos en las tinieblas.* <br> 
  [J.R.R Tolkien]{.flushright}<br>
  **Autor de la imagen:** Peter J. Yost
  **Licencia:** [CC-BY-SA-4.0](https://commons.wikimedia.org/wiki/Category:CC-BY-SA-4.0)
](assets/One_Ring_Blender_Render.png){width=80}

# Zer nolako fitxategiak sortzen ditu Edukik?

```
__build/
├── html/
│   ├── assets/
│   │   ├── dog.png
│   │   └── bird.png
│   ├── _eduki/
│   │   ├── eduki.css
│   │   ├── eduki.js
│   │   ├── edukilogo.png
│   │   └── icons.png
│   ├── eu/
│   │   ├── itry1/
│   │   │   ├── index.html
│   │   │   ├── 1-txoriak.html
│   │   │   ├── 2-ugaztunak.html
│   │   │   ├── 2.1-txakurrak.html
│   │   │   ├── slides.html
│   │   │   └── animaliak.pdf
│   │   ├── itry2/
│   │   │   ├── index.html
│   │   │   ├── 1-ugaztunak.html
│   │   │   ├── 1.1-txakurrak.html
│   │   │   ├── 2-txoriak.html
│   │   │   ├── slides.html
│   │   │   └── animaliak.pdf
│   │   └── index.html
│   ├── itry1/
│   │   ├── index.html
│   │   ├── 1-birds.html
│   │   ├── 2-mammals.html
│   │   ├── 2.1-dogs.html
│   │   ├── slides.html
│   │   └── animals.pdf
│   ├── itry2/
│   │   ├── index.html
│   │   ├── 1-mammals.html
│   │   ├── 1.1-dogs.html
│   │   ├── 2-birds.html
│   │   ├── slides.html
│   │   └── animals.pdf
│   ├── index.html
│   └── sitemap.json
└── tex/
    ├── assets/
    │   ├── dog.png
    │   └── bird.png
    ├── texfiles generated until the pdf is moved ...
    ├── animals.pdf
    └── animaliak.pdf
```  

Nola egiten du Edukik hau guztia? Edukik hurrengo kode fitxategien egitura erabitzen du horretarako.
```
animals
├── assets/
│   └── bird.png
├── 1.birds.md
├── 1.txoriak.eu.md
├── 2.mammals/
│   ├── img/
│   │   └── elephant.png
│   ├── 0.mammals.md
│   ├── 1.elephants.md
│   └── 1.elefanteak.md
└── index.md
```