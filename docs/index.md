---
# variables that deppend on the language
title: 'Guia: Eduki'
subtitle: Apuntes, resumen, presentaciones y sitio-web!
author: 'Iagoba Apellaniz'
affiliation: Departamento Matemática Aplicada (UPV-EHU)
license: '[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)'
date: February 21, 2024
abstract: |
  Eduki es una herramienta de creación de libros, apuntes y presentaciones.
  Permite la creación de todo esto.
description: |
  Página web creada con Eduki
lang: es
# Eduki variables
createslides: false
---

*Un solo código base para crear apuntes, resumenes, presentaciónes y sitio web. Un solo código base para distintos itinerarios y en multiples idiomas.*

![
  *Un Anillo para gobernarlos a todos.<br> Un Anillo para encontrarlos,<br>
un Anillo para atraerlos a todos y atarlos en las tinieblas.* <br> 
  [J.R.R Tolkien]{.flushright}<br>
  **Autor de la imagen:** Peter J. Yost
  **Licencia:** [CC-BY-SA-4.0](https://commons.wikimedia.org/wiki/Category:CC-BY-SA-4.0)
](assets/One_Ring_Blender_Render.png){width=80}

# Introducción 

El material docente generado cada año crece a medida que aumenta el número de herramientas y plataformas dedicadas a la enseñanza.
El típico libro o los apuntes están siendo (han sido) reemplazados por presentaciones, video-tutoriales, formularios y plataformas interactivas enfocadas a la enseñanza entre otras.
La coordinación y publicación de este material y el mantener la coherencia dentro de las actividades docentes se hace cada vez más dificil debido a la diversificación del material.

Eduki parte de la idea de aunar todos los esfuerzos en la creación de una plataforma didactica lineal, extensiva y extensible. 
Más en sintonía con los libros que con las actuales plataformas, el materíal creado con Eduki mantiene la linealidad necesaria en el proceso de aprendizaje.
Pese a mantener esta premisa, Eduki al tratarse de una plataforma web ofrece una alta flexibilidad a la hora de crear diferentes tipos de contenido.

En resumen, Eduki podría verse como un libro sobre la web, y no como un libro escrito soble el papel.
Es por ello que en Eduki se pueden insertar videos, paneles interactivos, proporciona una herramienta para lanzar presentaciones, y por último mantiene la opción de trasladar todo ello (en la medida de lo posible) al papel.

Por si esto fuera poco, con Eduki y partiendo siempre de un mismo código como se explica en la @sec:primeros-pasos, se podrán crear itinerarios diferentes para diferente audiencia y se podrá mantener de forma eficiente el material traducido a otros idiomas.

# Primeros pasos {#sec:primeros-pasos}

A la hora de crear contenido 

## Markdown



# Qué archivos produce Eduki?

Eduki crea la siguiente estructura de archivos.

```
__ html/
  ├── assets/
  │   ├── dog.png
  │   └── bird.png
  ├── _eduki/
  │   ├── eduki.css
  │   ├── eduki.js
  │   ├── edukilogo.png
  │   └── icons.png
  ├── eu/
  │   ├── itry1/
  │   │   ├── index.html
  │   │   ├── 1-txoriak.html
  │   │   ├── 2-ugaztunak.html
  │   │   ├── 2.1-txakurrak.html
  │   │   ├── slides.html
  │   │   └── animaliak.pdf
  │   ├── itry2/
  │   │   ├── index.html
  │   │   ├── 1-ugaztunak.html
  │   │   ├── 1.1-txakurrak.html
  │   │   ├── 2-txoriak.html
  │   │   ├── slides.html
  │   │   └── animaliak.pdf
  │   └── index.html
  ├── itry1/
  │   ├── index.html
  │   ├── 1-birds.html
  │   ├── 2-mammals.html
  │   ├── 2.1-dogs.html
  │   ├── slides.html
  │   └── animals.pdf
  ├── itry2/
  │   ├── index.html
  │   ├── 1-mammals.html
  │   ├── 1.1-dogs.html
  │   ├── 2-birds.html
  │   ├── slides.html
  │   └── animals.pdf
  ├── index.html
  └── sitemap.json

```  
Podemos encontrar las diferentes carpetas para diferentes itinerarios y idiomas. 
También se encuentra la carpeta `assets` donde se guardan archivos adjuntos al documento (generalment imágenes del propio documento).

¿Pero como hace todo esto `Eduki`? 
Eduki parte de un solo código fuente fara crear todo el contenido.
```
animals
├── assets/
│   ├── bird.png
│   └── elephant.png
├── 1.birds.md
├── 1.txoriak.eu.md
├── 2.mammals/
│   ├── 0.mammals.md
│   ├── 1.elephants.md
│   └── 1.elefanteak.md
└── index.md
```

# Contenidos propios de Eduki

Para la creación de contenido @eq:1 se utililiza el lenguaje Markdown de Pandoc. 
Por ejemplo {TODO:Add manual}.
Visto el siguiente ejemplo se comprenden fácilmente la mayoría de opciones que da Markdown. 

    # Capítulo

    ## Sección

    ### Subsección

    ## Segunda sección

La idea es mantener el código fuente del documento *lo más parecido al resultado final como sea posible*. 
De esta forma trabajar con el resulta más sencillo.

[
$$
E = mc^2
$$
]{#eq:1}

A pesar de la versatilidad que ofrece Markdown, y aprovechando la flexibilidad de "fenced blocks" o "fenced inlines" hemos añadido varios elementos.
A continuación veremos esos elementos.

## Teoremas

  :::{.theorem title}

  :::

  :::{.theorem }

## Notas {#notas}

    :::{.note title}
    Esto es una nota y se ve así
    :::

:::{.note}
Esto es una nota y se ve así
:::

### Opciones

Dentro de los bloques para notas hay lijeras variaciones como por ejemplo

    :::{.alert title}
    Esto es una nota y se ve así
    :::

::: {.pause}
:::

:::{.alert}
Esto es una nota y se ve así
:::

    :::{.warning title}
    Esto es una nota y se ve así
    :::

::: {.pause}
:::

:::{.warning}
Esto es una nota y se ve así
:::

    :::{.example title}
    Esto es una nota y se ve así
    :::

:::{.example}
Esto es una nota y se ve así 
:::

## Ejercicios

    :::{.exercise}
    Esto es un ejercicio.
    :::

:::{.exercise}
Esto es un ejercicio.
:::

Ejercicios con solución
    
    :::{.exercise}
    Esto es un ejercicio.
    
    :::{.hint}
    Su solución es esta.
    :::
    :::

:::{.exercise}
Esto es un ejercicio.

:::{.hint}
Su solución es esta.
:::
:::

## Iframes

  :::{.iframe max-width title}

  :::

# Itinerarios

La variable `rmvec`

Los itinerarios a seguir por el alumnado ayudan a mantener el código fuente de nuestros apuntes y con unos pocos ajusets ofrecer al alumnado notas y presentaciones ajustados a su itinerario académico.
Los itinerarios se configuran en `index.md`
```
itinerary:
  a: 
    title: "Primer itinerario"
    rmvec: ""
    permvec: ""
  b:
    title: "Segundo itinerario"
    rmvec: ""
    permvec: ""
```

El directorio de trabajo debe estar 

Los itinerarios se especifican en el archivo `config.md`

Por último, es posible también difereciar itinerarios mediante los bloques.
    
    :::{itry="a"}
    Testu hau euskerazko bertsioan agertuko da soilik.
    :::

    [Testu hau euskerazko bertsioan agertuko da soilik.]{itry="a"}


# Idiomas

Eduki compila una serie de apuntes basandose en la estructura de los archivos en la propia carpeta.
También distingue los archivos mediante su terminación.

Si el archivo termina en `.md` será considerado como parte de los apuntes en el idioma por defecto. 
Sin embargo, si el nombre del archivo tiene una terminación tal como `.eu.md` este archivo se considera que es parte de los apuntes en euskera. 

Por último, y dentro de los archivos por defecto, es posible también difereciar idiomas mediante los bloques.
    
    :::{lang="eu"}
    Testu hau euskerazko bertsioan agertuko da soilik.
    :::

    [Testu hau euskerazko bertsioan agertuko da soilik.]{lang="eu"}

# Presentaciones

Eduki crea por defecto una presentación del contenido.
Los límites de las diapositivas se autodefinen mediante las cabeceras de las secciones (independientemente del nivel).

             ┌ # Mis apuntes
    diapo. 1 | 
             | Apuntes sobre algo interesante.
             └ 
             ┌ ## Introducción
    diapo. 2 | 
             | En esta sección describimos la motivación principal para los apuntes
             └           
             ┌ ### Motivación
    diapo. 3 |
             | Estos apuntes son muy útiles para los estudiantes.
             └ 
             ┌ ## Método de estudio
    diapo. 4 |
             | El método de estudio recomendado son los apuntes.
             └ 
             ┌ # Bibliografía
    diapo. 5 | 
             | 1. John Smith (2024)
             └ 

## Límites adicionales y pausas

Para introducir nuevas diapositivas o incluso crear pausas durante la presentación, basta con utilizar las clases `.frame` o `.pause` creadas especificamente para ello.

             ┌ # Mis apuntes
    diapo. 1 | 
             | Apuntes sobre algo interesante.
             └ 
             ┌ ## Introducción
    diapo. 2 | 
             | En esta sección describimos la motivación principal para los apuntes.
             | Como la introducción es larga decidimos dividir la diapositiva en dos.
             └           
             ┌ []{.frame}
    diapo. 3 | 
             | La siguiente sección explica el metodo de estudio recomendado.
             └ 

::: {.frame}
:::

             ┌ ### Motivación
    diapo. 4 |
             | Estos apuntes son muy útiles para los estudiantes.
             └ 
             ┌ ### Método de estudio
    diapo. 5 |
    con      | * Cojer apuntes durante la presentación.
    pausas   | 
             | []{.pausa}
             | 
             | * Estudiar de los apuntes.
             └ 

::: {.pause}
:::

             ┌ # Bibliografía
    diapo. 6 | 
             | 1. John Smith (2024)
             └          
            
## Navegación y ajustes de la presentación

Cuando se lanza una presentación aparecerá un menú relacionado a la misma.

Botón | Acción
------|-------
<span class="material-symbols-rounded">first_page</span>    | Ir a la página anterior
<span class="material-symbols-rounded">stat_2</span>        | Ir a la primera diapositiva de la página
<span class="material-symbols-rounded">arrow_back</span>    | Ir a la diapositiva/pausa anterior
<span class="material-symbols-rounded">arrow_forward</span> | Ir a la siguente diapositiva/pausa
<span class="material-symbols-rounded">stat_minus_2</span>  | Ir a la última diapositiva de la página
<span class="material-symbols-rounded">last_page</span>     | Ir a la página siguiente
<span class="material-symbols-rounded">text_decrease</span> | Disminuir el tamaño de la fuente 
<span class="material-symbols-rounded">text_increase</span> | Aunemtar el tamaño de la fuente 

La presentación se expone en el propio navegador.
No se genera ningun archivo y dependerá de la pantalla/proyector utilizadas para su legibilidad.
Dependiendo del contenido, es posible que no se expongan de manera precisa.

Para ello, en la barra del menú apareceran varias opciones
