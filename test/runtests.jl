using Eduki, Pandoc, Test

@testset "Test parse identifiers, remove vector and permute vertor" begin
    @test (1,3)     == Eduki.parseidentifier("1.3")
    @test (1,false) == Eduki.parseidentifier("1.") # folder
    @test (1,true)  == Eduki.parseidentifier("1*") # all 
    @test (1,-1)    == Eduki.parseidentifier("1") # top level file
    @test [(1, 3),(2, true),(1, false)] == Eduki.parsermvec("1.3,2*,1.")
    @test [[(1, 3), (1, 5)], [(2, true), (3, true)]] ==  Eduki.parsepermvec("1.3|1.5,2*|3*")
    @test [[(1, 3), (1, 5)], [(2, true), (3, false)]] ==  Eduki.parsepermvec("1.3|1.5,2*|3.")
end

@testset "Test file names" begin
    @test true == Eduki.aredigits("123")
    @test false == Eduki.aredigits("1a3")

    @test true == Eduki.isvalidfilename("2.a.md")
    @test true == Eduki.isvalidfilename("a/2.a.md")
    @test true == Eduki.isvalidfilename("2.md")
    @test false == Eduki.isvalidfile("a.md")
    @test false == Eduki.isvalidfile("2.a")

    @test true == Eduki.isvalidfolder("2.a")
    
    @test "eu" == Eduki.getlangfromfilename("2.a.eu.md")
    @test "eu" == Eduki.getlangfromfilename("2.eu.md")
    @test "" == Eduki.getlangfromfilename("2.a.md")
end
