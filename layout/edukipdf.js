class MyHandler extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterPreview() {
        window.parent.postMessage('ready','*')
        document.body.classList.add("ready")
    }
}
Paged.registerHandlers(MyHandler);
