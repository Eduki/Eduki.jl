const puppeteer = require('puppeteer');

function sleep(time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(null)
        }, time)
    })
}

(async () => {

    const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    const page = await browser.newPage();

    await page.goto('http://127.0.0.1:8081/pdf.html', {
        waitUntil: 'networkidle2',
    });

    while (true) {
        const bodyClasses = await page.evaluate(`document.body.classList.contains("ready")`)
        if (bodyClasses) {
            break
        }
    }

    await page.pdf({
        path: '__node/tmp.pdf',
        format: 'a4'
    });

    await browser.close();
    
})();