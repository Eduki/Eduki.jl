---
# Crossref settings
linkReferences: true
chapters: true
chaptersDepth: 1
# Eduki settings
createpdf: true
createslides: true
---