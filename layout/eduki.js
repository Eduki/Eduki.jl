
window.MathJax = {
    loader: { load: ['[tex]/physics', '[tex]/color'] },
    tex: {
        packages: { '[+]': ['physics', 'color'] },
    },
};

window.onload = function () {
    var previousA = document.getElementById("previous")
    var nextA = document.getElementById("next")

    if (previousA && previousA.getAttribute("href") == "index.html") {
        previousA.parentElement.prepend(document.createElement("span"))
        previousA.remove()
    } else {
        document.getElementById("title-block-header").remove()
    }

    var hs = document.querySelectorAll("h1:not(.title), h2, h3, h4, h5, h6");
    var hlevels = [];
    for (let i = 0; i < hs.length; i++) {
        if (i == 0) {
            hs[i].classList.add("h1")
        }
        switch (hs[i].tagName) {
            case "H1":
                hlevels.push(1);
                break;
            case "H2":
                hlevels.push(2);
                break;
            case "H3":
                hlevels.push(3);
                break;
            case "H4":
                hlevels.push(4);
                break;
            case "H5":
                hlevels.push(5);
                break;
            case "H6":
                hlevels.push(6);
                break;
            default:
                break;
        }
    }
    hlevelmin = Math.min(...hlevels)
    hlevels = hlevels.map((el) => (el - hlevelmin) * 12)
    var pagetoc = document.getElementById("page-toc")
    var pagetocOL = document.createElement("ol")

    for (let i = 0; i < hs.length; i++) {
        var a = document.createElement("a");
        a.setAttribute("href", "#" + hs[i].id);
        aInHeader = a.cloneNode();
        a.style.marginLeft = hlevels[i] + "px"
        a.innerText = hs[i].innerText;
        aInHeader.innerHTML = '<span class="material-symbols-rounded">link</span>';
        aInHeader.style.display = "inline-block";
        aInHeader.style.transform = "translate(0, 0.2em)";
        hs[i].innerText += " ";
        hs[i].appendChild(aInHeader);
        var li = document.createElement("li");
        li.appendChild(a)
        pagetocOL.appendChild(li)
    }
    pagetoc.appendChild(pagetocOL)

    fetch('./sitemap.json')
        .then((response) => response.json())
        .then((sitemap) => {
            function lifromsection(section, depth) {
                var li = document.createElement("li")
                var a = document.createElement("a")
                var sm_path = encodeURI((section.section.path).split("#")[0])
                var w_paths = window.location.pathname.split("/")
                var w_path = w_paths[w_paths.length - 1]
                a.setAttribute("href", sm_path);
                if (sm_path == w_path) {
                    li.classList.add("current-page")
                    li.classList.add("expanded")
                }
                a.innerText = section.section.title;
                li.appendChild(a)
                if (depth && section.subsections.length > 0) {
                    var arrow = document.createElement("div")
                    arrow.innerHTML = '<div class="expand-arrow" onclick="toggleexpand(this)">🮦</div>'
                    li.appendChild(arrow)
                }
                return li
            }
            function olfromsection(sitemap, depth) {
                var ol = document.createElement("ol")
                var subsecs = sitemap.subsections
                for (var i = 0; i < subsecs.length; i++) {
                    ol.appendChild(lifromsection(subsecs[i], depth - 1))
                    if (subsecs[i].subsections.length > 0 && depth - 1) {
                        ol.appendChild(olfromsection(subsecs[i], depth - 1))
                    }
                }
                return ol
            }
            var sitemaproot = { subsections: [sitemap] }
            ol = olfromsection(sitemaproot, 3)
            var sitetoc = document.getElementById("sitemap")
            sitetoc.appendChild(ol)
            if (previousA.getAttribute("href") == "index.html") {
                ol.children[0].classList.add("current-page")
            }
            var pcli = ol.getElementsByClassName("current-page")[0].parentNode
            while (pcli.tagName == "OL") {
                let pspcli = pcli.previousSibling
                if (pspcli.tagName == "LI") {
                    pspcli.classList.add("expanded")
                }
                pcli = pcli.parentNode
            }
        })
        .catch((err) => { });

    redirects()
    setslidevariables()

    if (parseInt(localStorage.getItem("slides"))) {
        presentation()
        if (parseInt(localStorage.getItem("as-previous"))) {
            window.localStorage.setItem("as-previous", 0)
            lastslide()
        }
    }
    highlightsection()
    document.getElementById("grid").onscroll = function () {
        highlightsection()
    }
    document.getElementById("hide-all").style.display = "none"

    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var eventer = window[eventMethod];
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

    // Listen to message from child window
    eventer(messageEvent,function(e) {
        var key = e.message ? "message" : "data";
        var data = e[key];
        console.log('data')
        if (data == 'ready') {
            document.getElementById("pdf-frame").contentWindow.focus();
            document.getElementById("pdf-frame").contentWindow.print();
        }
    },false);
}

function getBody() {
    return document.getElementsByTagName("body")[0]
}

function highlightsection() {
    var hs = document.querySelectorAll("h1:not(.title), h2, h3, h4, h5, h6");
    for (var i = 0; i < hs.length; i++) {
        if (hs[i].getBoundingClientRect().top < parseFloat(window.getComputedStyle(getBody()).paddingTop) + 1) {
            continue
        } else {
            break
        }
    }
    if (i != 0) {
        i -= 1
    }
    lis = document.querySelectorAll("#page-toc li")
    for (let j = 0; j < lis.length; j++) {
        if (j == i) {
            lis[j].classList.add("current")
        } else {
            lis[j].classList.remove("current")
        }
    }
}

function toggleexpand(el) {
    var li = el.parentNode.parentNode
    if (li.classList.contains("expanded")) {
        li.classList.remove("expanded")
    } else {
        li.classList.add("expanded")
    }
}

function toggleleftbar() {
    var lbar = document.getElementById("left-bar")
    var rbar = document.getElementById("right-bar")
    var tarea = document.getElementById("toggle-shadow-area")
    if (rbar.classList.contains("showr")) {
        rbar.classList.remove("showr")
        tarea.classList.remove("showr")
        lbar.classList.add("showl")
        tarea.classList.add("showl")
        return
    }
    if (lbar.classList.contains("showl")) {
        lbar.classList.remove("showl")
        tarea.classList.remove("showl")
    } else {
        lbar.classList.add("showl")
        tarea.classList.add("showl")
    }
}
function togglerightbar() {
    var lbar = document.getElementById("left-bar")
    var rbar = document.getElementById("right-bar")
    var tarea = document.getElementById("toggle-shadow-area")
    if (lbar.classList.contains("showl")) {
        lbar.classList.remove("showl")
        tarea.classList.remove("showl")
        rbar.classList.add("showr")
        tarea.classList.add("showr")
        return
    }
    if (rbar.classList.contains("showr")) {
        rbar.classList.remove("showr")
        tarea.classList.remove("showr")
    } else {
        rbar.classList.add("showr")
        tarea.classList.add("showr")
    }
}
function escaperightleftbars() {
    var lbar = document.getElementById("left-bar")
    var rbar = document.getElementById("right-bar")
    var tarea = document.getElementById("toggle-shadow-area")
    lbar.classList.remove("showl")
    rbar.classList.remove("showr")
    tarea.classList.remove("showl")
    tarea.classList.remove("showr")
}

function gotocover() {
    // store temp variable gotocover = true and then go to index.html
}

function redirects() {
    // If index.html resume reading unless gotocover()
    p = window.location.pathname
    f = p.substr(p.lastIndexOf("/") + 1)
    if (f == 'index.html' || f == '') {
        window.location.href = document.getElementById("next").getAttribute("href")
    }
}

function setslideclases() {
    var els = document.querySelectorAll("h1,h2,h3,h4,h5")
    for (let i = 0; i < els.length; i++) {
        els[i].classList.add("frame")
    }
}

window.onresize = function () {
    setslidevariables()
}

function setslidevariables() {
    if (localStorage.getItem("scaled-fs")) {
        document.getElementsByTagName("body")[0].style.setProperty("--scaled-em", localStorage.getItem("scaled-fs"))
    } else {
        document.getElementsByTagName("body")[0].style.setProperty("--scaled-em", (window.innerWidth / 50 / 16).toString())
    }
}

function presentation() {
    document.getElementsByTagName("body")[0].classList.add("slides")
    setslideclases()
    var stops = document.querySelectorAll(".frame,.pause")
    gotocurrentslide()
    localStorage.setItem("slides", 1);
}
function stoppresentation() {
    document.getElementsByTagName("body")[0].classList.remove("slides");
    localStorage.setItem("slides", 0);
}
function nextslide() {
    var stops = document.querySelectorAll(".frame,.pause")
    var i = 0
    for (let j = 0; j < stops.length; j++) {
        if (stops[j].classList.contains("current-slide")) {
            i = j
        }
    }
    if (i < stops.length - 1) {
        stops[i + 1].classList.add("current-slide")
        stops[i].classList.remove("current-slide")
        if (stops[i + 1].classList.contains("frame")) {
            gotocurrentslide()
        }
    } else {
        var url = document.getElementById("next").getAttribute("href")
        window.location.href = url
    }
}
function previousslide() {
    var stops = document.querySelectorAll(".frame,.pause")
    var i = 0
    for (let j = 0; j < stops.length; j++) {
        if (stops[j].classList.contains("current-slide")) {
            i = j
        }
    }
    var k = 0
    if (i > 0) {
        stops[i - 1].classList.add("current-slide")
        stops[i].classList.remove("current-slide")
        if (stops[i].classList.contains("frame")) {
            gotocurrentslide()
        }
    } else {
        var url = document.getElementById("previous").getAttribute("href")
        window.location.href = url
        window.localStorage.setItem("as-previous", 1)
    }
}
function lastslide() {
    var stops = document.querySelectorAll(".frame,.pause")
    for (let i = 0; i < stops.length; i++) {
        stops[i].classList.remove("current-slide")
    }
    stops[stops.length - 1].classList.add("current-slide")
    gotocurrentslide()
}
function firstslide() {
    var stops = document.querySelectorAll(".frame,.pause")
    for (let i = 0; i < stops.length; i++) {
        stops[i].classList.remove("current-slide")
    }
    stops[0].classList.add("current-slide")
    gotocurrentslide()
}

function gotocurrentslide() {
    var stops = document.querySelectorAll(".frame,.pause")
    var j = 0
    for (let i = 0; i < stops.length; i++) {
        if (stops[i].classList.contains("current-slide")) {
            j = i
        }
        stops[i].classList.remove("current-frame")
    }
    var k = 0
    for (let i = 1; i <= j; i++) {
        if (stops[i].classList.contains("frame")) {
            k = i
        }
    }
    if (stops.length) { 
        stops[k].classList.add("current-frame") 
        if (k == 0) {
            stops[0].classList.add("current-slide")
        }
        var grid = document.getElementById("grid")
        grid.scrollBy(0, stops[k].getBoundingClientRect().top)
    }
    disablebuttons() 
}

function increasefontsize() {
    var body = document.getElementsByTagName("body")[0]
    var fs = parseFloat(body.style.getPropertyValue("--scaled-em")) * 1.1
    body.style.setProperty("--scaled-em", fs)
    localStorage.setItem("scaled-fs", fs)
    gotocurrentslide()

}
function decreasefontsize() {
    var body = document.getElementsByTagName("body")[0]
    var fs = parseFloat(body.style.getPropertyValue("--scaled-em")) / 1.1
    body.style.setProperty("--scaled-em", fs)
    localStorage.setItem("scaled-fs", fs)
    gotocurrentslide()
}
function previouspage() {
    var url = document.getElementById("previous").getAttribute("href")
    window.location.href = url
}
function nextpage() {
    var url = document.getElementById("next").getAttribute("href")
    window.location.href = url
}
function disablebuttons() {
    var stops = document.querySelectorAll(".frame,.pause")
    ap = document.getElementById("previous")
    an = document.getElementById("next")
    if (ap === null) {
        document.getElementById("previous-page").classList.add("disabled")
        if (stops.length) {
            if (stops[0].classList.contains("current-slide")) {
                document.getElementById("previous-slide").classList.add("disabled")
            } else {
                document.getElementById("previous-slide").classList.remove("disabled")
            }
        }
    } else {
        document.getElementById("previous-page").classList.remove("disabled")
    }
    if (an === null) {
        document.getElementById("next-page").classList.add("disabled")
        if (stops[stops.length - 1].classList.contains("current-slide")) {
            document.getElementById("next-slide").classList.add("disabled")
        } else {
            document.getElementById("next-slide").classList.remove("disabled")
        }
    } else {
        document.getElementById("next-page").classList.remove("disabled")
    }
}

document.addEventListener('keydown', handlekeypress);

function handlekeypress(e) {
    if (getBody().classList.contains('slides')) {
        switch (e.key) {
            case "Up":
            case "ArrowUp":
                previouspage()
            break
            case "Down":
            case "ArrowDown":
                nextpage()
            break
            case "Left":
            case "ArrowLeft":
                previousslide()
            break
            case "Right":
            case "ArrowRight":
                nextslide()
            break
            case " ":
            case "Spacebar":
                nextslide()
            break
            case "Escape":
                stoppresentation()
            break
            default:
                return
        }
    }
}