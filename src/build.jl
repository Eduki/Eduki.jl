"""
    build()

Build the entire project.
"""
function build(;baseurl = "", dev = false) 

    @info "Reading INDEX MD file ..."
    isfile("index.md") || error("There should be an \"index.md\" file ...")

    # Default indexdoc. Read first default Eduki md file.
    indexdoc = Pandoc.Document(read(abspathfileineduki("layout/eduki.md"), String))
    joindocs!(indexdoc, Pandoc.Document(read("index.md", String)))
    
    @info "Geting default metadata ..."
    meta = Dict(JSON3.read(JSON3.write(indexdoc.meta.content)))
    ks = Symbol[]
    for (k, v) in meta
        push!(ks, k)
    end
    lang    = in(:lang, ks) ? meta[:lang][:c][1][:c] : nothing
    rmvec   = in(:rmvec, ks) ? parsermvec(meta[:rmvec][:c][1][:c]) : nothing
    itrys   = in(:itinerary, ks) ? meta[:itinerary][:c] : nothing # :url => :title, :rmvec, :permvec
    createpdf   = in(:createpdf, ks) ? meta[:createpdf][:c] : false 

    # Get the list of the rest of the languages from index files names
    langs = getlangs()
    # Get actual files and parse them

    @info "Reading MD files ..."
    docs = getdocs() # DataFrame( :lang, :chap, :sec, :doc )

    outdir = joinpath(HTML_DIR, baseurl)
    @show dev
    if dev
        cpfrompkgtodir("layout/eduki.js", joinpath(HTML_DIR, "lib"))
        cpfrompkgtodir("layout/eduki.css", joinpath(HTML_DIR, "lib"))
        cpfrompkgtodir("layout/edukilogo.png", joinpath(HTML_DIR, "lib"))
    end
    if createpdf
        installpuppeter()
        cpfrompkgtodir("layout/eduki.css", joinpath("__pdf", "lib"))
        cpfrompkgtodir("layout/edukilogo.png", joinpath("__pdf", "lib"))
        cpfrompkgtodir("layout/edukipdf.css", joinpath("__pdf", "lib"))
        cpfrompkgtodir("layout/edukipdf.js", joinpath("__pdf", "lib"))
    end

    # Build whole site for default lang and default itrinerary 
    buildsite(getdoc(
        indexdoc = indexdoc, 
        docs = docs,
        lang = nothing, 
        title = nothing, 
        rmvec = rmvec,
        permvec = nothing,
    ), outdir, createpdf)

    # Build for all itineraries and for the default language
    if !isnothing(itrys)
        for url in keys(itrys)
            rmvec = length(itrys[url][:c][:rmvec][:c]) == 0 ? nothing : parsermvec(itrys[url][:c][:rmvec][:c][1])
            permvec = length(itrys[url][:c][:permvec][:c]) == 0 ? nothing : parsepermvec(itrys[url][:c][:permvec][:c][1])
            buildsite(getdoc(
                indexdoc = indexdoc, 
                docs = docs,
                lang = nothing, 
                title = itrys[url][:c][:title][:c], # Pandoc object
                rmvec = rmvec,
                permvec = permvec,
            ), joinpath(outdir, string(url)), createpdf)
        end
    end

    # Build for langs
    for l in langs

        ldir = joinpath(outdir, l)

        indexdoc = joindocs(indexdoc, Pandoc.Document("index." * l * ".md"))
        meta = Dict(JSON3.read(JSON3.write(indexdoc.meta.content)))
        
        rmvec   = in(:rmvec, ks) ? parsermvec(meta[:rmvec][:c][1][:c]) : nothing
        itrys   = in(:itinerary, ks) ? meta[:itinerary][:c] : nothing
        
        buildsite(getdoc(
            indexdoc = indexdoc, 
            docs = docs,
            lang = l, 
            title = nothing, 
            rmvec = rmvec,
            permvec = nothing,
        ), ldir, createpdf)

        if !isnothing(itrys)
            for url in keys(itrys)
                rmvec = length(itrys[url][:c][:rmvec][:c]) == 0 ? nothing : parsermvec(itrys[url][:c][:rmvec][:c][1])
                permvec = length(itrys[url][:c][:permvec][:c]) == 0 ? nothing : parsepermvec(itrys[url][:c][:permvec][:c][1])
                buildsite(getdoc(
                    indexdoc = indexdoc, 
                    docs = docs,
                    lang = l, 
                    title = itrys[url][:c][:title][:c], # Pandoc object
                    rmvec = rmvec,
                    permvec = permvec,
                ), joinpath(ldir, string(url)), createpdf)
            end
        end
    end

    nothing

end

function buildsite(doc, outdir, createpdf)
    # this command runs from ./, i.e. the working directory
    # it removes all the files and the assets folder in ./outdir
    if isdir(outdir) 
        for f in readdir(outdir, join=true)
            if isfile(f) || basename(f) == "assets"
                rm(f, recursive=true)
            end
        end
    else
        mkpath(outdir)
    end
    # Pandoc cannot overwrite when chunkedhtml is used
    # Create a tmp dir and compile there. Then, mv the files and folders.
    tmp = tempname()
    run(Pandoc.Converter(
        to = "chunkedhtml", 
        input = doc,
        output = tmp,
        chunk_template = "%n.html",
        split_level = 2,
        mathjax=true,
        number_sections=false,
        filter=["$(CROSSREF_BIN_PATH)"],
        template=abspathfileineduki("layout/eduki.html")
    ))
    for f in readdir(tmp, join=true)
        mv(f, joinpath(outdir, basename(f)), force=true)
    end
    if createpdf
        run(Pandoc.Converter(
            to = "html", 
            input = doc,
            output = joinpath("__pdf", "pdf.html"),
            mathjax=true,
            number_sections=true,
            filter=["$(CROSSREF_BIN_PATH)"],
            template=abspathfileineduki("layout/edukipdf.html")
        ))
        cp(joinpath(outdir,"assets"), "__pdf/assets", force=true)
        printtopdf(outdir)
    end
end

function cpfrompkgtodir(pkgrpath, outdir)
    mkpath(outdir)
    cp(abspathfileineduki(pkgrpath), joinpath(outdir, basename(pkgrpath)), force=true)
end

function abspathfileineduki(rpath)
    joinpath(normpath(joinpath(pathof(Eduki), "../..")), rpath)
end

function getdoc(;
    indexdoc = indexdoc, 
    docs = docs, # DataFrame( :lang, :chap, :sec, :doc )
    lang = nothing, 
    title = nothing, 
    rmvec = nothing,
    permvec = nothing,
)   
    docs = deepcopy(docs)
    # filter first for lang or nothing
    filter!([:lang] => (l) -> isnothing(l) || l == lang, docs)
    # filter for rmvec [1,2], [1,-1] or [1,true], [1,false]
    
    if !isnothing(rmvec)
        for rmpair in rmvec
            if typeof(rmpair[2]) == Int
                filter!([:chap, :sec] => (c, s) -> !(c == rmpair[1] && s == rmpair[2]), docs)
            elseif rmpair[2]
                filter!([:chap] => (c) -> !(c == rmpair[1]), docs)
            else
                filter!([:chap, :sec] => (c, s) -> !(c == rmpair[1] && s != -1), docs)
            end
        end
    end
    # Take default docs => if there are two in chap-sec with lang = nothing and lang = lang, choose lang)
    chaps = unique(docs.chap)
    secs = unique(docs.sec)
    for chap in chaps, sec in secs
        row_or_rows = filter([:chap, :sec] => (c, s) -> c == chap && s == sec, docs)
        if nrow(row_or_rows) == 2
            filter!([:lang, :chap, :sec] => (l, c, s) -> l == lang && c == chap && s == sec, docs)
        end
    end
    if !isnothing(permvec)
        for (frompair, topair) in permvec
            typeof(frompair[2]) == typeof(topair[2]) || error()
            if typeof(frompair[2]) == Int # file to file
                for (i, row) in enumerate(eachrow(docs))
                    if row.chap == frompair[1] && row.sec == frompair[2]
                        docs.chap[i] = topair[1]
                        docs.sec[i] = topair[2]
                    elseif row.chap == topair[1] && row.sec == topair[2]
                        docs.chap[i] = frompair[1]
                        docs.sec[i] = frompair[2]
                    end
                end
            elseif !(frompair[2] || topair[2])  # folder to folder
                for (i, row) in enumerate(eachrow(docs))
                    if row.chap == frompair[1] && row.sec >= 0
                        docs.chap[i] = topair[1]
                    elseif row.chap == topair[1] && row.sec >= 0
                        docs.chap[i] = frompair[1]
                    end
                end
            elseif frompair[2] && frompair[2] # chap to chap
                for (i, row) in enumerate(eachrow(docs))
                    if row.chap == frompair[1]
                        docs.chap[i] = topair[1]
                    elseif row.chap == topair[1]
                        docs.chap[i] = frompair[1]
                    end
                end
            end
        end
    end
    # Join docs
    doc = Pandoc.Document()
    if !isnothing(lang)
        indexlangdoc = Pandoc.Document(read("index." * lang * ".md", String))
        doc.meta.content = recursive_merge(indexdoc.meta.content, indexlangdoc.meta.content)
        doc.blocks = indexlangdoc.blocks
    else
        joindocs!(doc, indexdoc)
    end
    filterprevious!(doc)
    for (i, row) in enumerate(eachrow(docs))
        if row.sec > 0
            shiftheadinglevels!(docs.doc[i], 1)
        end
        joindocs!(doc, docs.doc[i])
    end
    if !isnothing(title)
        # include itry_title variable 
    end
    filterpost!(doc)
    doc
end

function installpuppeter()
    isdir("__node") || mkdir("__node")
    cd("__node")
    run(`$(node()) $npm i puppeteer`)
    cp(abspathfileineduki("layout/edukiprintpdf.js"), "edukiprintpdf.js", force=true)
    cd("..")
end

function serve!(basedir)
    server = HTTP.serve!() do req::HTTP.Request
        req.target == "/" && return HTTP.Response(200, read(joinpath(basedir,"index.html")))
        file = req.target[2:end]
        return isfile(joinpath(basedir,file)) ? HTTP.Response(200, read(joinpath(basedir,file))) : HTTP.Response(404)
    end
    server
end

function printtopdf(outdir)
    server = serve!("__pdf")
    run(`$(node()) __node/edukiprintpdf.js`)
    close(server)
    mv("__node/tmp.pdf", joinpath(outdir, "document.pdf"))
end