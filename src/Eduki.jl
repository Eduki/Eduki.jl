__precompile__(true)

module Eduki

using Pandoc, JSON3, Countries, pandoc_crossref_jll, DataFrames, NodeJS_20_jll, HTTP

PAbstractPath = Pandoc.FilePathsBase.AbstractPath

CROSSREF_BIN_PATH = pandoc_crossref_jll.pandoc_crossref_path

LANG_CODES = map((el)-> el.alpha2 ,filter((el) -> !isnothing(el.alpha2), all_languages()))

HTML_NOTE_CLASSES = ["alert", "warning", "example"]

HTML_DIR = joinpath("__html")

include("build.jl")
include("mdfiles.jl")
include("pandoc.jl")
include("serve.jl")

end # module
