function getdocs()
    fs = getfiles()
    chaps = Int[]
    secs = Int[]
    langs = Union{Nothing,String}[]
    docs = Pandoc.Document[]
    for f in fs
        sp = splitpath(f)
        push!(chaps, parse(Int, split(sp[1], ".")[1]))
        length(sp) == 1 ? push!(secs, -1) : push!(secs, parse(Int, split(sp[2], ".")[1]))
        push!(langs, getlangfromfilename(basename(f)))
        doc = Pandoc.Document(read(f, String), from="markdown")
        filterprevious!(doc)
        push!(docs, doc)
    end
    df = DataFrame(lang = langs, chap = chaps, sec = secs, doc = docs)
    sort!(df, [:chap, :sec])
    df
end

function parseidentifier(str)
    str[end] == '*' && return (parse(Int, str[1:end-1]), true)
    str[end] == '.' && return (parse(Int, str[1:end-1]), false)
    str[1]   == '.' && return (false, parse(Int, str[2:end]))
    nm = parse.(Int, split(str, "."))
    if length(nm) == 1
        return (nm[1], -1)
    else
        return (nm[1], nm[2])
    end
end
function parsermvec(rmstr)
    parseidentifier.(split(rmstr, ",", keepempty = false))
end
function parsepermvec(permstr)
    map((str_vec) -> parseidentifier.(str_vec), 
        split.(split(permstr,",", keepempty = false),"|")
    )
end
function aredigits(str)
    str == "" && return false
    for c in str
        isdigit(c) && continue
        return false
    end
    true
end
function isvalidfilename(filename)
    splitedname = split(filename, ".")
    if aredigits(splitedname[1]) && lowercase(splitedname[end]) == "md"
        return true
    else
        return false
    end
end
function isvalidfolder(foldername)
    splitedname = split(foldername, ".")
    if aredigits(splitedname[1])
        return true
    else
        return false
    end
end
function getlangfromfilename(filename)
    l = lowercase(split(filename, ".")[end-1])
    if l in LANG_CODES 
        return l
    else
        nothing
    end
end
function getfiles()
    paths = String[]
    for (root, dirs, files) in walkdir(".")
        # filter root for valid folders
        root == "." || reduce( * , isvalidfolder.(splitpath(root))[2,end]) || continue
        # filter files for valid ones
        push!(paths, map((p) -> normpath(joinpath(root, p)), filter(isvalidfilename, files))...)
    end
    paths
end

"Get langs from index filenames"
function getlangs()
    o = String[]
    fs = readdir()
    for f in fs
        m = match(r"^index\.([a-z]{2})\.md$", f)
        if !isnothing(m)
            push!(o, m.captures[1])
        end
    end
    o     
end