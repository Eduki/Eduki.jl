function servesite(;dir="./", launch_rebuid=false)
    cd(dir)
    if rebuild
        build()
    end
    serve()
end