recursive_merge(x::AbstractDict...) = merge(recursive_merge, x...)
recursive_merge(x::AbstractVector...) = cat(x...; dims=1)
recursive_merge(x...) = x[end]

"Join docs by merging their meta and concatenating their blocks. Returns a new doc."
function joindocs(doc1::Pandoc.Document, doc2::Pandoc.Document)
    o = Pandoc.Document()
    o.meta.content = recursive_merge(doc1.meta.content, doc2.meta.content)
    o.blocks = vcat(deepcopy(doc1.blocks), deepcopy(doc2.blocks))
    o
end
"Join docs by merging their meta and concatenating their blocks. Mutates the first doc."
function joindocs!(doc1::Pandoc.Document, doc2::Pandoc.Document)
    doc1.meta.content = recursive_merge(doc1.meta.content, doc2.meta.content)
    doc1.blocks = vcat(doc1.blocks, deepcopy(doc2.blocks))
    doc1
end
"Remove blocks and return a new doc"
function emptydoc(doc::Pandoc.Document)
    o = Pandoc.Document() 
    o.meta.content = copy(doc.meta.content)
    o
end
"Remove blocks of current doc"
function emptydoc!(doc)
    doc.blocks = Pandoc.Block[]
    doc
end

"Create a pandoc block from a string"
blockfromstring(str) = Pandoc.Document(str).blocks
"Create an pandoc inline from a string"
inlinefromstring(str) = Pandoc.Document(str).blocks[1].content

# FILTERS
function filterprevious!(doc)
    addnoteclass!(doc)
end
function filterpost!(doc)
    
end

function addnoteclass!(doc)
    for c in HTML_NOTE_CLASSES
        Pandoc.addclass!.(Pandoc.elementsbyclass(Pandoc.PandocNode(doc), c), "note")
    end
end
function shiftheadinglevels!(doc, n)
    hs = Pandoc.elementsbytype(Pandoc.PandocNode(doc), Pandoc.Header)
    for h in hs
        h.level += n
    end
end